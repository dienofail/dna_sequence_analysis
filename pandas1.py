# -*- coding: utf-8 -*-
"""
Created on Mon Dec 02 23:15:48 2013

@author: AlvinDesktop
"""

import numpy as np
import matplotlib as mpl
#from pandas import Series, DataFrame
import pandas

handle = 'ANOVA-commoncombinedfinal.csv'

abc = pandas.io.parsers.read_csv(handle)
f= lambda x: x.max() - x.min()
result = abc.sort(['Mean(N_0)', 'Mean(N_4)'], ascending=[1,0])
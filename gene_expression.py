import numpy as npy
from decimal import Decimal
import os
import matplotlib.pyplot as plt
import Pycluster as pyclust
import csv


print "Thyis git is working!"
#handle = 'common_combined_Alexei_clustering_input.txt';
handle = 'ANOVA-commoncombinedfinal.txt'
# for line in handle:
#     elements = line.rstrip("\n")
#     myvector = elements.split("\t")

clusters = 30
cluster_dict = dict()
cols = []
output = {k: [] for k in range(clusters)}
Data_array = []
namesoutput = {k: [] for k in range(clusters)}
print "My output is of format" + str(type(output))
print "And my size of format is " + str(len(output))
for x in range(3,19):
    cols.append(x)
linecounter = 0
import re
with open(handle) as f:
    next(f)
    for line in f:
        #print line
        elements = line.rstrip() 
        elements = line.strip()
        currentline = elements.split("\t")
        array_length = len(currentline)
        #print currentline[3:]
    #     print currentline
#         cluster_dict[linecounter] = currentline[1]
#         linecounter += 1 
        my_bool = 0
        my_bool2 = 0
        if Decimal(currentline[2]) > 1.8:
            my_bool2 = 1
        for idx, val in enumerate(currentline):
            val = val.strip()
            if idx < 3:
                next
            elif idx < 20:
                m = re.match(r'\d+', val)
                if m:
                    #print str(m)
                    #print "\n"
    #                 print "Current value is " + str(val)
                    whats_left = Decimal(val) - Decimal(currentline[2])
    #                 print Decimal(val)
    #                 print Decimal(currentline[2])
    #                 print "\n"
                    if abs(whats_left) > 0.176:
                        my_bool = 1
        if my_bool == 1 & my_bool2 == 1:
            Data_array.append(currentline[3:19])
            #print Data_array
            cluster_dict[linecounter] = currentline[array_length-2]
            linecounter += 1 

# with open(handle, 'r') as f:
#     line = f.readline()
#     elements = line.rstrip("\n")
#     print elements


####Initalize the matrix####
#from numpy import loadtxt
# data = loadtxt(handle, delimiter="\t", usecols = cols, skiprows = 1)
data = npy.array(Data_array)
#print data
clusterid, error, nfound = pyclust.kcluster(data, nclusters=clusters, npass = 1, dist='c')
copy_of_data = data.tolist()
counter = 0
#print clusterid


myfile = open('clusters_correleation_output.txt', 'w')
clusteridlist = clusterid.tolist()
for idx, val in enumerate(clusteridlist):
    print val
    #myfile.write(str(val) + "\t" + str(cluster_dict[idx]) + "\n")
    namesoutput[val].append(cluster_dict[idx])
for key in namesoutput:
    #print namesoutput[key]
    myfile.write(str(key))
    for idx, val in enumerate(namesoutput[key]):
        myfile.write("\t" + str(val))
    myfile.write("\n")
myfile.close()



for idx, val in enumerate(copy_of_data):
    #print abc[x][0]
    counter += 1
    #print "Current cluster id is " + str(clusterid[idx]) + "\n" + "my value is"
    #print val
    output[clusterid[idx]].append(val)

averages = []
for key in output:
    current_list = output[key]
    print key
    print "\n"
    print output[key]
    current_array = output[key]
    print "\n"
#     current_npy_array = npy.array(output[key])
#     current_average = npy.mean(current_npy_array, axis = 0)
#     to_push_average = current_average.tolist()
#     averages.append(to_push_average)
#     print to_push_average
    f, (N_plot, CM_plot, EM_plot, EC_plot) = plt.subplots(4, sharex = True)
    f_2, (N_average_plot, CM_average_plot, EM_average_plot, EC_average_plot) = plt.subplots(4,sharex = True)
    f.hold(True)
    N_plot.set_title('N at cluster' + str(key))
    CM_plot.set_title('CM')
    EM_plot.set_title('EM')
    EC_plot.set_title('EC')
    my_x_axis = npy.arange(4)
    for idx, val in enumerate(current_array):
        array = val
        current_npy_array = npy.array(array)
        N_y_axis = current_npy_array[0:4]
        CM_y_axis = current_npy_array[4:8]
        EM_y_axis = current_npy_array[8:12]
        EC_y_axis = current_npy_array[12:]
        N_plot.plot(my_x_axis,N_y_axis)
        CM_plot.plot(my_x_axis,CM_y_axis)
        EM_plot.plot(my_x_axis,EM_y_axis)
        EC_plot.plot(my_x_axis,EC_y_axis)
        current_npy_array = npy.array(array)
        current_average = npy.mean(current_npy_array)
        normalized_npy_array = [x-current_average for x in current_npy_array]
        N_average_axis = normalized_npy_array[0:4]
        CM_average_axis = normalized_npy_array[4:8]
        EM_average_axis = normalized_npy_array[8:12]
        EC_average_axis = normalized_npy_array[12:]
        N_plot.plot(my_x_axis,N_y_axis)
        CM_plot.plot(my_x_axis,CM_y_axis)
        EM_plot.plot(my_x_axis,EM_y_axis)
        EC_plot.plot(my_x_axis,EC_y_axis)
    f.subplots_adjust(hspace=0.5)
    EC_plot.set_xticks(npy.arange(4))
    EC_plot.set_xticklabels(('0','4','16','72'))
    f.savefig(str(key) + '.png', dpi=300)
    
